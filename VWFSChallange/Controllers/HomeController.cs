﻿using System.Collections.Generic;
using System.Web.Mvc;
using VWFSChallange.Models;

namespace VWFSChallange.Controllers
{
    public class HomeController : Controller
    {
        // Hardcoded list of Vehicles
        // The brief says this is valid rather than signing up for a lookup service
        List<VehicleDetails> VehicleLookup = new List<VehicleDetails>
        {
            new VehicleDetails {Registration="AB17MMM", Make="Volkswagen", Model="Scirroco", Color="Black", Doors=3, Engine="2.5 TDI" },
            new VehicleDetails {Registration="BR17AFD", Make="Volkswagen", Model="Golf", Color="Red", Doors=3, Engine="1.0 TDI" },
            new VehicleDetails {Registration="NM17DJF", Make="Volkswagen", Model="Golf", Color="White", Doors=3, Engine="1.2 S" },
            new VehicleDetails {Registration="OX17EEF", Make="Audi", Model="Q5", Color="White", Doors=5, Engine="3.0 M" },
            new VehicleDetails {Registration="KY17NMR", Make="Audi", Model="S7", Color="Black", Doors=3, Engine="2.0 TDI" },
            new VehicleDetails {Registration="AB17HBK", Make="Audi", Model="A3", Color="Red", Doors=3, Engine="2.0 TDI" },
            new VehicleDetails {Registration="KV10YZM", Make="Volkswagen", Model="Golf", Color="Grey", Doors=5, Engine="2.0 TDI" }
        };

        // The welcome/intro page
        public ActionResult Index()
        {
            return View();
        }

        // GET requests for the apply page
        public ActionResult Apply()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Apply(VehicleDetails postData)
        {
            // If the user has submitted some data, follow the vehicle found root
            if (postData.Registration != "")
            {
                return VehicleFound(postData);
            }
            else
            {
                // Just follow the standard apply page
                return View("Apply");
            }
            
        }

        // What to display for GET requests
        public ActionResult VehicleFound()
        {
            ViewBag.Message = "Vehicle Found";

            return View();
        }

        [HttpPost]        
        public ActionResult VehicleFound(VehicleDetails postData)
        {
            ViewBag.Message = "Vehicle Found";
            ViewBag.PostData = postData;

            // If we have a registration to look up, add the results to the model and show the vehicle found page
            if (postData.Registration != null)
            {
                string Reg = postData.Registration.Replace(" ", "").ToUpper();
                var myCar = VehicleLookup.Find(car => car.Registration == Reg);
                return View("VehicleFound", myCar);
            }
            else
            {
                // This actually returns the fact we've recorded the milage
                // If the brief wanted more stages covered I'd have broken this out into another Action
                return View("VehicleFound", postData);
            }
            
        }

        // MVC scaffold creates this by default
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        // MVC scaffold creates this by default
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        // Json helper methods, could be called by Angular or jQuery

        // Get a specific vehicle by registration
        public JsonResult GetVehicleInfo(string Reg)
        {
            return Json(VehicleLookup.Find(car => car.Registration == Reg), JsonRequestBehavior.AllowGet);
        }

        // List out all of our hard coded vehicles
        public JsonResult GetVehicleList()
        {
            return Json(VehicleLookup, JsonRequestBehavior.AllowGet);
        }


    }
}