﻿if (!jQuery) { throw new Error("Bootstrap requires jQuery"); }

// Uses jQuery matchHeight (external codebase) to ensure all cards are the same size.
$(function () {
    $(".card").matchHeight();
});