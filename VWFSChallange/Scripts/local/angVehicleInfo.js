﻿var VehicleInfoApp = angular.module("VehicleInfoApp", ['ngAnimate']);

VehicleInfoApp.controller("VehicleInfoController", function ($scope, VehicleInfoService) {
    $scope.getRegistrationList = function() {
        VehicleInfoService.getRegistrationList()
            .then(
                function (dets) {
                    $scope.details = dets;
                    console.log($scope.details);
                },
                function (error) {
                    $scope.status = 'Unable to retrieve registration help: ' + error.message;
                    console.log($scope.status);
                }
            );
    };
});

VehicleInfoApp.factory("VehicleInfoService", ["$http", function ($http) {

    var vehicleInfoService = {};
    // If the site were to be more modal, we could use GetVehicleInfo to receive the 
    // vehicle details and display them on the page
    vehicleInfoService.getCarDetails = function ($registration) {
        return $http.get("/Home/GetVehicleInfo/" + $registration);
    };

    // Pulls back the hard-coded list of vehicles to make it easier to see
    // the coding challenge in action
    vehicleInfoService.getRegistrationList = function () {
        return $http.get("/Home/GetVehicleList");
    }
    return vehicleInfoService;

}]);  