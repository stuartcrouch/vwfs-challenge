﻿// A small application to show the stages and current progress.
var StagesApp = angular.module("StagesApp", ["ngAnimate"]);

StagesApp.controller("StagesController", function ($scope) {
    $scope.stages = [
        { id: 1, title: 'Vehicle Details' },
        { id: 2, title: 'Plans' },
        { id: 3, title: 'Personal Details' },
        { id: 4, title: 'Summary' },
        { id: 5, title: 'Payment' },
        { id: 6, title: 'Confirmation' }
    ];
    $scope.activeStage = 1; // This would be pulled from the system data in a full app.
});