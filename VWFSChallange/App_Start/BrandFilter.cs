﻿using System.Web.Mvc;

namespace VWFSChallange
{
    internal class BrandFilter : ActionFilterAttribute
    {
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            // Add the product name to the ViewBag.
            // This could be pulled out of web.config, making an easy way to rebrand just by changing a config file.
            filterContext.Controller.ViewBag.BrandName = "Volkswagen";
        }
    }
}