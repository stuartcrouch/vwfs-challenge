﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VWFSChallange.Models
{
    // All the details used in the brief.
    public class VehicleDetails
    {
        // Adding data annotations allows us to use jQuery to validate the form before submission
        // as well as validating server-side before the form is passed into the controller
        [Required]
        [RegularExpression("^(([A-Za-z]{1,2}[ ]?[0-9]{1,4})|([A-Za-z]{3}[ ]?[0-9]{1,3})|([0-9]{1,3}[ ]?[A-Za-z]{3})|([0-9]{1,4}[ ]?[A-Za-z]{1,2})|([A-Za-z]{3}[ ]?[0-9]{1,3}[ ]?[A-Za-z])|([A-Za-z][ ]?[0-9]{1,3}[ ]?[A-Za-z]{3})|([A-Za-z]{2}[ ]?[0-9]{2}[ ]?[A-Za-z]{3})|([A-Za-z]{3}[ ]?[0-9]{4}))$", ErrorMessage = "Invalid registration number format")]
        public string Registration { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Engine { get; set; }
        public int Doors { get; set; }
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid mileage")]
        public int Mileage { get; set; }
    }
}